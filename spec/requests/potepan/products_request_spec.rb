require 'rails_helper'

RSpec.describe "Potepan::Products", type: :request do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create(:product, taxons: [taxon]) }

  before do
    get potepan_product_path(product.id)
  end

  it 'showのhttpリクエストが成功する' do
    expect(response).to be_successful
  end

  it 'showとパーシャルがレンダリングされる' do
    expect(response).to render_template :show
    expect(response).to render_template(partial: '_related_products')
    expect(response).to render_template(partial: '_light_section')
  end

  it '商品名が表示される' do
    expect(response.body).to include product.name
  end

  it '商品の値段が表示される' do
    expect(response.body).to include product.display_price.to_s
  end

  it '商品の説明が表示される' do
    expect(response.body).to include product.description
  end

  it '関連商品名が表示される' do
    expect(response.body).to include related_product.name
  end

  it '関連商品の値段が表示される' do
    expect(response.body).to include related_product.display_price.to_s
  end

  describe "Potepan::Products::Related_products" do
    let!(:related_products) { create_list(:product, Const::RELATED_PRODUCTS_MAX_COUNT + 1, taxons: [taxon]) }

    it '関連商品が等しい数だけ表示される' do
      get potepan_product_path(product.id)
      expect(assigns(:related_products).size).to eq Const::RELATED_PRODUCTS_MAX_COUNT
    end
  end
end
