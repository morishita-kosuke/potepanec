require 'rails_helper'

RSpec.describe "Potepan::Categories", type: :request do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    get potepan_category_path(taxon.id)
  end

  it 'showのhttpリクエストが成功する' do
    expect(response).to be_successful
  end

  it 'showとパーシャルがレンダリングされる' do
    expect(response).to render_template :show
    expect(response).to render_template(partial: '_light_section_top')
    expect(response).to render_template(partial: '_light_section_bottom')
    expect(response).to render_template(partial: '_sidebar')
    expect(response).to render_template(partial: '_filter_area')
  end

  it 'taxonが表示される' do
    expect(response.body).to include taxon.name
  end

  it 'taxonomyが表示される' do
    expect(response.body).to include taxonomy.name
  end

  it '商品名が表示される' do
    expect(response.body).to include product.name
  end
end
