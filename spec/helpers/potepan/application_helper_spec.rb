require 'rails_helper'

RSpec.describe "ApplicationHelperTest", type: :helper do
  include ApplicationHelper

  describe "#full_title(page_title)" do
    it "page_titleの値が空である" do
      expect(full_title("")).to eq Const::BASE_TITLE
    end

    it "page_titleの値がnilである" do
      expect(full_title(nil)).to eq Const::BASE_TITLE
    end

    it "page_titleの値がexampleである" do
      expect(full_title("example")).to eq "example | #{Const::BASE_TITLE}"
    end
  end
end
