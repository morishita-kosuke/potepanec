require 'rails_helper'

RSpec.feature "Categories/show.html.erb", type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent: taxonomy.root) }
  let!(:product) { create(:product, taxons: [taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it '適切なカテゴリータイトルが表示される' do
    expect(page).to have_title "#{taxon.name} | #{Const::BASE_TITLE}"
  end

  it 'カテゴリーリストが表示される' do
    within '.side-nav' do
      expect(page).to have_content taxonomy.name
      expect(page).to have_content taxon.name
    end
  end

  it 'カテゴリーリストをクリックするとカテゴリーページへ移動' do
    within '.side-nav' do
      click_on taxon.name
      expect(current_path).to eq potepan_category_path(taxon.id)
    end
  end

  it 'カテゴリー名と値段が表示される' do
    within '.productBox' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
    end
  end

  it '商品をクリックすると商品ページへ移動' do
    within '.productBox' do
      click_link product.name
      expect(current_path).to eq potepan_product_path(product.id)
    end
  end

  it '適切なカテゴリー名が表示される' do
    within '.pageHeader' do
      expect(page).to have_selector 'h2', text: taxon.name
      expect(page).to have_selector 'li', text: taxon.name
    end
  end
end
