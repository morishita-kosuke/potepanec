require 'rails_helper'

RSpec.feature "Products/show.html.erb", type: :feature do
  let(:taxon) { create(:taxon) }
  let(:product) { create(:product, taxons: [taxon]) }
  let!(:related_product) { create_list(:product, Const::RELATED_PRODUCTS_MAX_COUNT + 1, taxons: [taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  it '各商品毎にタイトル、商品名、値段、説明が表示される' do
    expect(page).to have_title "#{product.name} | #{Const::BASE_TITLE}"
    within '.media-body' do
      expect(page).to have_content product.name
      expect(page).to have_content product.display_price
      expect(page).to have_content product.description
    end
  end

  it '一覧ページへ戻るリンクが正常に動作する' do
    expect(page).to have_link "一覧ページへ戻る"
    click_link "一覧ページへ戻る"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  it '関連商品が等しい数だけ表示される' do
    expect(page).to have_selector ".productBox", count: Const::RELATED_PRODUCTS_MAX_COUNT
  end

  it '関連商品をクリックすると、クリックした関連商品詳細ページへ移動' do
    click_on related_product[0].name
    expect(current_path).to eq potepan_product_path(related_product[0].id)
  end
end
