require 'rails_helper'

RSpec.describe 'Potepan::Products', type: :model do
  let(:taxon) { create(:taxon) }
  let(:taxonomy) { create(:taxonomy) }
  let(:product) { create(:product, taxons: [taxon]) }
  let(:example_taxon) { create(:taxon, name: "example", parent: taxonomy.root, taxonomy: taxonomy) }
  let(:example_product) { create(:product, name: "example-mug", taxons: [example_taxon]) }

  it "関連商品にメインの商品は含まれない" do
    expect(product.related_products).not_to include product
  end

  it "関連しない商品は含まれない" do
    expect(product.related_products).not_to include example_product
  end
end
