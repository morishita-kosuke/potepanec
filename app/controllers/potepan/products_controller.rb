class Potepan::ProductsController < ApplicationController
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = @product.related_products.
      includes(master: [:default_price, :images]).limit(Const::RELATED_PRODUCTS_MAX_COUNT)
  end
end
